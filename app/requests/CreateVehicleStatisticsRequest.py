from pydantic import BaseModel


class CreateVehicleStatisticsRequest(BaseModel):
    vehicle_id: int
    fuel: int
    mileage: int
