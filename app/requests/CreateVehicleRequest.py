from pydantic import BaseModel


class CreateVehicleRequest(BaseModel):
    name: str
    vehicle_name: str
    weight: float
