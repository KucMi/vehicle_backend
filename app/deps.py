from app.services.VehicleService import VehicleService
from app.services.VehicleStatisticsService import VehicleStatisticsService
from database import async_session


async def get_vehicle_service():
    async with async_session() as session:
        async with session.begin():
            yield VehicleService(session)


async def get_vehicle_statistics_service():
    async with async_session() as session:
        async with session.begin():
            yield VehicleStatisticsService(session)
