import datetime
from typing import Optional

from sqlmodel import SQLModel, Field
import sqlalchemy as sa


class Vehicle(SQLModel, table=True):
    __tablename__ = "vehicles"

    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(nullable=False, unique=True)
    vehicle_name: str = Field(nullable=False)
    weight: float = Field(nullable=False)
    created_at: datetime.datetime = Field(
        sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.datetime.utcnow))
