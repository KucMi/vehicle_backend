from datetime import datetime
from typing import Optional

from sqlmodel import SQLModel, Field
import sqlalchemy as sa

from app.models.Vehicle import Vehicle


class VehicleStatistics(SQLModel, table=True):
    __tablename__ = "vehicle_statistics"

    id: Optional[int] = Field(default=None, primary_key=True)
    vehicle_id: int = Field(
        sa_column=sa.Column(sa.Integer, sa.ForeignKey(Vehicle.id, onupdate="CASCADE", ondelete="CASCADE")))

    fuel: float = Field(nullable=False)
    mileage: float = Field(nullable=False)

    created_at: Optional[datetime] = Field(
        sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.utcnow))

    updated_at: Optional[datetime] = Field(
        sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.utcnow))
