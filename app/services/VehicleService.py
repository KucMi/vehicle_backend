from sqlmodel import Session, select

from app.models.Vehicle import Vehicle
from app.requests.CreateVehicleRequest import CreateVehicleRequest


class VehicleService:
    def __init__(self, session: Session):
        self.session = session

    async def create(self, data: CreateVehicleRequest) -> Vehicle:
        vehicle = Vehicle(name=data.name, vehicle_name=data.vehicle_name, weight=data.weight)

        self.session.add(vehicle)
        await self.session.commit()

        return vehicle

    async def get(self, vehicle_id: int) -> None:
        query = (
            select(Vehicle)
            .where(Vehicle.id == vehicle_id)
        )

        result = await self.session.execute(query)
        return result.scalars().one()
