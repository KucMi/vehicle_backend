from sqlmodel import Session

from app.models.VehicleStatistics import VehicleStatistics
from app.requests.CreateVehicleStatisticsRequest import CreateVehicleStatisticsRequest


class VehicleStatisticsService:
    def __init__(self, session: Session):
        self.session = session

    async def create(self, data: CreateVehicleStatisticsRequest) -> VehicleStatistics:
        new_data = VehicleStatistics(vehicle_id=data.vehicle_id, fuel=data.fuel, mileage=data.mileage)

        self.session.add(new_data)
        await self.session.commit()

        return new_data
