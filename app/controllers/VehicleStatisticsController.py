from fastapi import APIRouter, Body, Depends
from starlette import status
from starlette.responses import Response

from app.deps import get_vehicle_statistics_service
from app.requests.CreateVehicleStatisticsRequest import CreateVehicleStatisticsRequest
from app.services.VehicleStatisticsService import VehicleStatisticsService

vehicle_statistics_router = APIRouter(
    prefix="/vehicles/statistics",
    tags=["Vehicles Statistics"]
)


@vehicle_statistics_router.post("/")
async def create_statistics_vehicle(*,
                                    vehicle_statistics_service: VehicleStatisticsService = Depends(
                                        get_vehicle_statistics_service),
                                    data: CreateVehicleStatisticsRequest = Body()
                                    ):
    await vehicle_statistics_service.create(data)

    return Response(status_code=status.HTTP_201_CREATED)
