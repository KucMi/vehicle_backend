from fastapi import APIRouter, Depends, Body
from starlette import status
from starlette.responses import Response

from app.deps import get_vehicle_service
from app.requests.CreateVehicleRequest import CreateVehicleRequest
from app.services.VehicleService import VehicleService

vehicle_router = APIRouter(
    prefix="/vehicles",
    tags=["Vehicles"]
)


@vehicle_router.post("/")
async def create_vehicle(*,
                         vehicle_service: VehicleService = Depends(get_vehicle_service),
                         create_vehicle_data: CreateVehicleRequest = Body()
                         ):
    await vehicle_service.create(create_vehicle_data)

    return Response(status_code=status.HTTP_201_CREATED)


@vehicle_router.get("/{vehicle_id}")
async def get_vehicle(
        *,
        vehicle_service: VehicleService = Depends(get_vehicle_service),
        vehicle_id: int,
):
    vehicle = await vehicle_service.get(vehicle_id)

    return vehicle
