from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.controllers.VehicleController import vehicle_router
from app.controllers.VehicleStatisticsController import vehicle_statistics_router
from database import init_db

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def on_startup():
    await init_db()


@app.get("/")
async def index():
    return {"status": "Api is running"}

# vehicle.com/api/v1/vehicles [POST]

app.include_router(vehicle_router, prefix="/api/v1")
app.include_router(vehicle_statistics_router, prefix="/api/v1")
